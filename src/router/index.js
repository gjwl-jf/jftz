import Vue from 'vue'
import VueRouter from 'vue-router'
const Home = () => import('../views/home/Home.vue')
//热点推荐
const HotRecommend=()=> import('../views/home/childComps/HotRecommend.vue')
//历史推荐
const HistoryRecommend=()=> import('../views/home/childComps/HistoryRecommend.vue')
const MessageCenter = () => import('../views/messageCenter/MessageCenter.vue')

const Market = () => import('../views/market/Market.vue')
const Trading = () => import('../views/trading/Trading.vue')

//我的
const Profile = () => import('../views/profile/Profile.vue')
//联系客服
const ContactCustomerService = ()=>import('../views/profile/childComps/ContactCustomerService.vue')
//资金明细
const MoneyDetail = () => import('../views/profile/childComps/MoneyDetail.vue')
//我的钱包
const MyWallet = () => import('../views/profile/childComps/MyWallet.vue')
//钱包提取
const Extract = () => import('../views/profile/childComps/myWalletChildComps/Extract.vue')
//进账查询
const InComeQuery = () => import('../views/profile/childComps/myWalletChildComps/InComeQuery.vue')
//提取记录
const Record = () => import('../views/profile/childComps/myWalletChildComps/Record.vue')
//个人信息
const UserInfo = () => import('../views/profile/childComps/UserInfo.vue')
const ChangeNickname = () => import('../views/profile/childComps/userInfoChildComps/ChangeNickname.vue')
const PersonalData = () => import('../views/profile/childComps/userInfoChildComps/PersonalData.vue')
const RealnameAuthentication = () => import('../views/profile/childComps/userInfoChildComps/RealnameAuthentication.vue')
const Authentication = ()=>import('../views/profile/childComps/userInfoChildComps/Authentication.vue')
//设置
const Setting = () => import('../views/setting/Setting.vue')
const DisplaySettings = () => import('../views/setting/childComp/DisplaySettings.vue')
const SoundSettings = () => import('../views/setting/childComp/SoundSettings.vue')
const ChangePassword = () => import('../views/setting/childComp/ChangePassword.vue')


//观点
const Viewpoint = () => import('../views/viewpoint/Viewpoint.vue')
//提问
const Question = () => import('../views/viewpoint/childComps/Question.vue')

//登录模块
const Login = () => import('../views/login/Login.vue')
//忘记密码
const ForgetPwd=()=>import('../views/login/childComps/ForgetPwd.vue')
//开户
const OpenAccount = () => import('../views/login/childComps/OpenAccount.vue')
//客户协议
const ClientAgreement = () => import('../views/login/childComps/ClientAgreement.vue')
//风险披露
const RiskDisclosure = () => import('../views/login/childComps/RiskDisclosure.vue')
//免责声明
const Disclaimer = () => import('../views/login/childComps/Disclaimer.vue')


const FeedBack = () => import('../views/feedback/FeedBack.vue')
const OnlineConsulting = () => import('../views/onlineConsulting/OnlineConsulting.vue')
//存款
const CheckStand = () => import('../views/checkstand/CheckStand.vue')
const Stand = () => import('../views/stand/Stand.vue')

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		redirect: '/home'
	},
	{
		path: '/home',
		component: Home,
		meta: { showTabbar: true }
	},
	//热点推荐
	{
		path: '/hotRecommend',
		component: HotRecommend,
		meta: { showTabbar: false }
	},
	//历史推荐
	{
		path: '/historyRecommend',
		component: HistoryRecommend,
		meta: { showTabbar: false }
	},
	//消息中心
	{
		path: '/messageCenter',
		component: MessageCenter,
		meta: { showTabbar: false }
	},
	//行情
	{
		path: '/market',
		component: Market,
		meta: { showTabbar: true }
	},
	//交易
	{
		path: '/trading',
		component: Trading,
		meta: { showTabbar: true }
	},
	//观点
	{
		path: '/viewpoint',
		component: Viewpoint,
		meta: { showTabbar: true }
	},
	//提问
	{
		path: '/question',
		component: Question,
		meta: { showTabbar: false }
	},
	//我的 
	{
		path: '/profile',
		component: Profile,
		meta: { showTabbar: true }
	},
	//资金明细
	{
		path: '/moneyDetail',
		component: MoneyDetail,
		meta: { showTabbar: false }
	},
	//我的钱包
	{
		path: '/myWallet',
		component: MyWallet,
		meta: { showTabbar: false }
	},
	//钱包提取
	{
		path: '/extract',
		component: Extract,
		meta: { showTabbar: false }
	},
	//进账查询
	{
		path: '/inComeQuery',
		component: InComeQuery,
		meta: { showTabbar: false }
	},
	//提取记录
	{
		path: '/record',
		component: Record,
		meta: { showTabbar: false }
	},
	//登录
	{
		path: '/login',
		component: Login,
		meta: { showTabbar: false }
	},
	//忘记密码
	{
		path: '/forgetPwd',
		component: ForgetPwd,
		meta: { showTabbar: false }
	},
	//开户
	{
		path: '/openAccount',
		component: OpenAccount,
		meta: { showTabbar: false }
	},
	//客户协议
	{
		path: '/clientAgreement',
		component: ClientAgreement,
		meta: { showTabbar: false }
	},
	//风险披露
	{
		path: '/riskDisclosure',
		component: RiskDisclosure,
		meta: { showTabbar: false }
	},
	//免责声明
	{
		path: '/disclaimer',
		component: Disclaimer,
		meta: { showTabbar: false }
	},
	//设置
	{
		path: '/setting',
		component: Setting,
		meta: { showTabbar: false }
	},
	{
		path: '/displaySettings',
		component: DisplaySettings,
		meta: { showTabbar: false }
	},
	{
		path: '/soundSettings',
		component: SoundSettings,
		meta: { showTabbar: false }
	},
	{
		path: '/changePassword',
		component: ChangePassword,
		meta: { showTabbar: false }
	},
	//联系客服
	{
		path: '/contactCustomerService',
		component: ContactCustomerService,
		meta: { showTabbar: false }
	},
	//个人信息模块
	{
		path: '/userInfo',
		component: UserInfo,
		meta: { showTabbar: false }
	},
	//昵称
	{
		path: '/changeNickname',
		component: ChangeNickname,
		meta: { showTabbar: false }
	},
	//个人资料
	{
		path: '/personalData',
		component: PersonalData,
		meta: { showTabbar: false }
	},
	//实名认证
	{
		path: '/realnameAuthentication',
		component: RealnameAuthentication,
		meta: { showTabbar: false }
	},
	//身份认证
	{
		path: '/authentication',
		component: Authentication,
		meta: { showTabbar: false }
	},
	//意见反馈
	{
		path: '/feedback',
		component: FeedBack,
		meta: { showTabbar: false }
	},
	{
		path: '/onlineConsulting',
		component: OnlineConsulting,
		meta: { showTabbar: false }
	},
	//存款
	{
		path: '/checkstand',
		component: CheckStand,
		meta: { showTabbar: false }
	},
	{
		path: '/stand',
		component: Stand,
		meta: { showTabbar: false }
	},
]

const router = new VueRouter({
	routes
})

export default router

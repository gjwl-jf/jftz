import Axios from 'axios'
import router from '../router/index'
import $qs from 'qs'
// 请求拦截器
Axios.interceptors.request.use(config => {
	let token = localStorage.getItem("token");
	if (token) {
		config.headers.common['token'] = token;
	}
    return config;
}, error => {
    return Promise.reject(error)
})
// 响应拦截器
Axios.interceptors.response.use(response => {
    // 对响应数据做一些事情
    return response;
}, error => {
	
    // 请求错误时做些事
    let status = ''
    if(error.request){
        status = error.request
    }else if(error.response){
        status = error.response
    }
	
    if (status) {
        switch (status.status) {
            case 400: error.message = '请求错误(400)'; 
				error.response = JSON.parse(status.response)
                break;
            case 401: error.message = '未授权，请重新登录(401)';
					error.response = JSON.parse(status.response)
					router.push('/login')
                break;
            case 403: error.message = '拒绝访问(403)';
					error.response = JSON.parse(status.response)
                break;
            case 404: error.message = '请求出错(404)'; 
                    error.response = JSON.parse(status.response)
                break;
            case 408: error.message = '请求超时(408)'; 
				error.response = JSON.parse(status.response)
                break;
            case 500: error.message = '服务器错误(500)'; 
				error.response = JSON.parse(status.response)
                      // router.push('/500')
                break;
            case 501: error.message = '服务未实现(501)'; 
				error.response = JSON.parse(status.response)
                break;
            case 502: error.message = '网络错误(502)'; 
				error.response = JSON.parse(status.response)
                break;
            case 503: error.message = '服务不可用(503)'; 
				error.response = JSON.parse(status.response)
                break;
            case 504: error.message = '网络超时(504)'; 
				error.response = JSON.parse(status.response)
                break;
            case 505: error.message = 'HTTP版本不受支持(505)';
				error.response = JSON.parse(status.response)
                break;
            default: error.message = `连接出错(${error.response.status})!`;
        }
    }else{
        error.message ='连接服务器失败!'
    }
	
    return Promise.reject(error)

})

export default Axios

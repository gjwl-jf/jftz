import axios from 'axios';
import $qs from 'qs';
import {Toast} from 'vant';
export default async(url = '', data = {}, type = 'GET', method = 'fetch') => {
	return new Promise((resolve, reject) => {
		axios({
		  method:type,
		  url: url, 
		  data: data
		}).then(response => {
	      resolve(response.data);
	    }).catch((err) =>{
			Toast({
			  message: err.response.msg,
			  position: 'bottom',
			});
			return;
		})
	})


}
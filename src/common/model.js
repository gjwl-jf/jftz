import http from '@/common/http';
import {Toast} from 'vant';
import router from '@/router';
import fetch from '@/common/fetch';

//登录 
export const login = (username,password,system) => fetch('/v1/login/login/login', {
	username:username,
	password:password,
	system:system
}, 'POST');


// 注册
export const reg = (mobile,sms,password,invite) => fetch('/v1/login/reg/sms?XDEBUG_SESSION_START=16275', {
	mobile:mobile,
	sms:sms,
	password:password,
	invite:invite
}, 'POST');

// 发送验证码
export const sms = (mobile) => fetch('/v1/login/sms/reg', {
	mobile:mobile
}, 'POST');


// 个人中心 信息
export const user = () => fetch('/v1/user/user/index', {}, 'GET');


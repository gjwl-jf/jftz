import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "./plugins/vant.js"
import 'vant/lib/index.less';
// import Axios from 'axios';
import Axios from './common/axiosConfig'
Vue.prototype.$axios = Axios;
Axios.defaults.baseURL = '/api'
import http from './common/http'
Vue.prototype.$http = http
import model from './common/model.js'
Vue.prototype.$model = model
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

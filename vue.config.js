// vue.config.js
module.exports = {
    css: {
      loaderOptions: {
        less: {
          // 若 less-loader 版本小于 6.0，请移除 lessOptions 这一级，直接配置选项。
          lessOptions: {
            modifyVars: {
              // 直接覆盖变量
              'nav-bar-icon-color':"#333",
              'nav-bar-text-color':"#333",
              'nav-bar-title-font-size':'18px'
            //   'text-color': '#111',
            //   'border-color': '#eee',
              // 或者可以通过 less 文件覆盖（文件路径为绝对路径）
            //   hack: `true; @import "your-less-file-path.less";`,width: 72px;
            },
          },
        },
      },
    },
	devServer: {
	    host: 'localhost',
	    port: 8080,
	    proxy: {
	      '/api': {
	        target: 'http://43.255.29.96',// 要跨域的域名
	        changeOrigin: true, // 是否开启跨域
	      }
	    }
	  }
  };